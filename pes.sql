--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.14
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE public.auth_group OWNER TO pesuser;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.auth_group_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_id_seq OWNER TO pesuser;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.auth_group_id_seq OWNED BY public.auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_group_permissions OWNER TO pesuser;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.auth_group_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_group_permissions_id_seq OWNER TO pesuser;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.auth_group_permissions_id_seq OWNED BY public.auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE public.auth_permission OWNER TO pesuser;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.auth_permission_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_permission_id_seq OWNER TO pesuser;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.auth_permission_id_seq OWNED BY public.auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(150) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE public.auth_user OWNER TO pesuser;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE public.auth_user_groups OWNER TO pesuser;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.auth_user_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_groups_id_seq OWNER TO pesuser;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.auth_user_groups_id_seq OWNED BY public.auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.auth_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_id_seq OWNER TO pesuser;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.auth_user_id_seq OWNED BY public.auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE public.auth_user_user_permissions OWNER TO pesuser;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.auth_user_user_permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auth_user_user_permissions_id_seq OWNER TO pesuser;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.auth_user_user_permissions_id_seq OWNED BY public.auth_user_user_permissions.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE public.django_admin_log OWNER TO pesuser;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.django_admin_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_admin_log_id_seq OWNER TO pesuser;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.django_admin_log_id_seq OWNED BY public.django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE public.django_content_type OWNER TO pesuser;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.django_content_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_content_type_id_seq OWNER TO pesuser;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.django_content_type_id_seq OWNED BY public.django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE public.django_migrations OWNER TO pesuser;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.django_migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.django_migrations_id_seq OWNER TO pesuser;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.django_migrations_id_seq OWNED BY public.django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE public.django_session OWNER TO pesuser;

--
-- Name: pes_app_carrera; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.pes_app_carrera (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    facultad_id integer NOT NULL
);


ALTER TABLE public.pes_app_carrera OWNER TO pesuser;

--
-- Name: pes_app_carrera_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.pes_app_carrera_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pes_app_carrera_id_seq OWNER TO pesuser;

--
-- Name: pes_app_carrera_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.pes_app_carrera_id_seq OWNED BY public.pes_app_carrera.id;


--
-- Name: pes_app_ciudad; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.pes_app_ciudad (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL
);


ALTER TABLE public.pes_app_ciudad OWNER TO pesuser;

--
-- Name: pes_app_ciudad_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.pes_app_ciudad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pes_app_ciudad_id_seq OWNER TO pesuser;

--
-- Name: pes_app_ciudad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.pes_app_ciudad_id_seq OWNED BY public.pes_app_ciudad.id;


--
-- Name: pes_app_facultad; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.pes_app_facultad (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    universidad_id integer NOT NULL
);


ALTER TABLE public.pes_app_facultad OWNER TO pesuser;

--
-- Name: pes_app_facultad_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.pes_app_facultad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pes_app_facultad_id_seq OWNER TO pesuser;

--
-- Name: pes_app_facultad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.pes_app_facultad_id_seq OWNED BY public.pes_app_facultad.id;


--
-- Name: pes_app_tipo_usuario; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.pes_app_tipo_usuario (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL
);


ALTER TABLE public.pes_app_tipo_usuario OWNER TO pesuser;

--
-- Name: pes_app_tipo_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.pes_app_tipo_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pes_app_tipo_usuario_id_seq OWNER TO pesuser;

--
-- Name: pes_app_tipo_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.pes_app_tipo_usuario_id_seq OWNED BY public.pes_app_tipo_usuario.id;


--
-- Name: pes_app_universidad; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.pes_app_universidad (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    ciudad_id integer NOT NULL
);


ALTER TABLE public.pes_app_universidad OWNER TO pesuser;

--
-- Name: pes_app_universidad_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.pes_app_universidad_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pes_app_universidad_id_seq OWNER TO pesuser;

--
-- Name: pes_app_universidad_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.pes_app_universidad_id_seq OWNED BY public.pes_app_universidad.id;


--
-- Name: pes_app_usuario; Type: TABLE; Schema: public; Owner: pesuser
--

CREATE TABLE public.pes_app_usuario (
    id integer NOT NULL,
    nombre character varying(100) NOT NULL,
    apellidos character varying(100) NOT NULL,
    rut character varying(10) NOT NULL,
    correo character varying(25) NOT NULL,
    contrasena character varying(1000) NOT NULL,
    carrera_id integer,
    tipo_usuario_id integer
);


ALTER TABLE public.pes_app_usuario OWNER TO pesuser;

--
-- Name: pes_app_usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: pesuser
--

CREATE SEQUENCE public.pes_app_usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pes_app_usuario_id_seq OWNER TO pesuser;

--
-- Name: pes_app_usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: pesuser
--

ALTER SEQUENCE public.pes_app_usuario_id_seq OWNED BY public.pes_app_usuario.id;


--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_group ALTER COLUMN id SET DEFAULT nextval('public.auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_permission ALTER COLUMN id SET DEFAULT nextval('public.auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_user ALTER COLUMN id SET DEFAULT nextval('public.auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_user_groups ALTER COLUMN id SET DEFAULT nextval('public.auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('public.auth_user_user_permissions_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.django_admin_log ALTER COLUMN id SET DEFAULT nextval('public.django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.django_content_type ALTER COLUMN id SET DEFAULT nextval('public.django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.django_migrations ALTER COLUMN id SET DEFAULT nextval('public.django_migrations_id_seq'::regclass);


--
-- Name: pes_app_carrera id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_carrera ALTER COLUMN id SET DEFAULT nextval('public.pes_app_carrera_id_seq'::regclass);


--
-- Name: pes_app_ciudad id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_ciudad ALTER COLUMN id SET DEFAULT nextval('public.pes_app_ciudad_id_seq'::regclass);


--
-- Name: pes_app_facultad id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_facultad ALTER COLUMN id SET DEFAULT nextval('public.pes_app_facultad_id_seq'::regclass);


--
-- Name: pes_app_tipo_usuario id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_tipo_usuario ALTER COLUMN id SET DEFAULT nextval('public.pes_app_tipo_usuario_id_seq'::regclass);


--
-- Name: pes_app_universidad id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_universidad ALTER COLUMN id SET DEFAULT nextval('public.pes_app_universidad_id_seq'::regclass);


--
-- Name: pes_app_usuario id; Type: DEFAULT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_usuario ALTER COLUMN id SET DEFAULT nextval('public.pes_app_usuario_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add log entry	1	add_logentry
2	Can change log entry	1	change_logentry
3	Can delete log entry	1	delete_logentry
4	Can view log entry	1	view_logentry
5	Can add permission	2	add_permission
6	Can change permission	2	change_permission
7	Can delete permission	2	delete_permission
8	Can view permission	2	view_permission
9	Can add group	3	add_group
10	Can change group	3	change_group
11	Can delete group	3	delete_group
12	Can view group	3	view_group
13	Can add user	4	add_user
14	Can change user	4	change_user
15	Can delete user	4	delete_user
16	Can view user	4	view_user
17	Can add content type	5	add_contenttype
18	Can change content type	5	change_contenttype
19	Can delete content type	5	delete_contenttype
20	Can view content type	5	view_contenttype
21	Can add session	6	add_session
22	Can change session	6	change_session
23	Can delete session	6	delete_session
24	Can view session	6	view_session
25	Can add usuario	7	add_usuario
26	Can change usuario	7	change_usuario
27	Can delete usuario	7	delete_usuario
28	Can view usuario	7	view_usuario
29	Can add carrera	8	add_carrera
30	Can change carrera	8	change_carrera
31	Can delete carrera	8	delete_carrera
32	Can view carrera	8	view_carrera
33	Can add ciudad	9	add_ciudad
34	Can change ciudad	9	change_ciudad
35	Can delete ciudad	9	delete_ciudad
36	Can view ciudad	9	view_ciudad
37	Can add facultad	10	add_facultad
38	Can change facultad	10	change_facultad
39	Can delete facultad	10	delete_facultad
40	Can view facultad	10	view_facultad
41	Can add tipo_usuario	11	add_tipo_usuario
42	Can change tipo_usuario	11	change_tipo_usuario
43	Can delete tipo_usuario	11	delete_tipo_usuario
44	Can view tipo_usuario	11	view_tipo_usuario
45	Can add universidad	12	add_universidad
46	Can change universidad	12	change_universidad
47	Can delete universidad	12	delete_universidad
48	Can view universidad	12	view_universidad
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$120000$ItjJFt0b9zaB$Z6D1LAMoDbRQi7oCB1/JDgLjdgbZ2mPupaGKKAov28c=	2018-11-23 13:40:53.359335-03	t	admin				t	t	2018-11-21 21:17:23.463235-03
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2018-11-21 21:35:00.231357-03	1	claudio	1	[{"added": {}}]	7	1
2	2018-11-21 21:35:12.241421-03	1	claudio	2	[]	7	1
3	2018-11-21 21:38:20.138178-03	1	claudio	2	[]	7	1
4	2018-11-23 13:41:19.545669-03	1	claudio	2	[]	7	1
5	2018-11-30 11:40:01.461113-03	1	Talca	1	[{"added": {}}]	9	1
6	2018-11-30 11:40:21.645033-03	1	UTALCA	1	[{"added": {}}]	12	1
7	2018-11-30 11:40:40.268079-03	1	FACULTAD INGENIERIA	1	[{"added": {}}]	10	1
8	2018-11-30 11:40:55.918009-03	1	Bioinfo	1	[{"added": {}}]	8	1
9	2018-11-30 11:41:24.613499-03	1	Profesor	1	[{"added": {}}]	11	1
10	2018-11-30 11:41:30.970497-03	2	Alumno	1	[{"added": {}}]	11	1
11	2018-11-30 11:41:36.364767-03	3	Ayudante	1	[{"added": {}}]	11	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.django_content_type (id, app_label, model) FROM stdin;
1	admin	logentry
2	auth	permission
3	auth	group
4	auth	user
5	contenttypes	contenttype
6	sessions	session
7	pes_app	usuario
8	pes_app	carrera
9	pes_app	ciudad
10	pes_app	facultad
11	pes_app	tipo_usuario
12	pes_app	universidad
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-11-21 21:16:59.986837-03
2	auth	0001_initial	2018-11-21 21:17:01.077112-03
3	admin	0001_initial	2018-11-21 21:17:01.376074-03
4	admin	0002_logentry_remove_auto_add	2018-11-21 21:17:01.430531-03
5	admin	0003_logentry_add_action_flag_choices	2018-11-21 21:17:01.482207-03
6	contenttypes	0002_remove_content_type_name	2018-11-21 21:17:01.542312-03
7	auth	0002_alter_permission_name_max_length	2018-11-21 21:17:01.57562-03
8	auth	0003_alter_user_email_max_length	2018-11-21 21:17:01.608092-03
9	auth	0004_alter_user_username_opts	2018-11-21 21:17:01.63687-03
10	auth	0005_alter_user_last_login_null	2018-11-21 21:17:01.67592-03
11	auth	0006_require_contenttypes_0002	2018-11-21 21:17:01.686623-03
12	auth	0007_alter_validators_add_error_messages	2018-11-21 21:17:01.716998-03
13	auth	0008_alter_user_username_max_length	2018-11-21 21:17:01.855827-03
14	auth	0009_alter_user_last_name_max_length	2018-11-21 21:17:01.888142-03
15	sessions	0001_initial	2018-11-21 21:17:02.132903-03
16	pes_app	0001_initial	2018-11-21 21:30:13.286061-03
17	pes_app	0002_auto_20181129_2134	2018-11-29 18:34:47.284935-03
18	pes_app	0003_auto_20181206_1336	2018-12-06 13:37:05.298073-03
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.django_session (session_key, session_data, expire_date) FROM stdin;
vqu12yylp8toaf7yx2uvee6ctzoxvr4z	YTNiOTkzOTU3OTljMTRlMGQ2M2NhMTA4ZGZhYTlkNWM3NzRmZGIwNzp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJlNTU2Mzk2YzJlZjdkNzMzOTc3MzVmN2FiMTM5MzQ1ZmVkODQzZDM2In0=	2018-12-07 13:40:53.425838-03
\.


--
-- Data for Name: pes_app_carrera; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.pes_app_carrera (id, nombre, facultad_id) FROM stdin;
1	Bioinfo	1
\.


--
-- Data for Name: pes_app_ciudad; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.pes_app_ciudad (id, nombre) FROM stdin;
7	Curico
21	Santiago
1	Talca
\.


--
-- Data for Name: pes_app_facultad; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.pes_app_facultad (id, nombre, universidad_id) FROM stdin;
1	FACULTAD INGENIERIA	1
3	FACULTAD MEDICINA	1
\.


--
-- Data for Name: pes_app_tipo_usuario; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.pes_app_tipo_usuario (id, nombre) FROM stdin;
1	Profesor
2	Alumno
3	Ayudante
\.


--
-- Data for Name: pes_app_universidad; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.pes_app_universidad (id, nombre, ciudad_id) FROM stdin;
1	UTALCA	1
4	UCHILE	21
5	USACH	21
\.


--
-- Data for Name: pes_app_usuario; Type: TABLE DATA; Schema: public; Owner: pesuser
--

COPY public.pes_app_usuario (id, nombre, apellidos, rut, correo, contrasena, carrera_id, tipo_usuario_id) FROM stdin;
1	claudio	quevedo	18427102-8	claudio.quev92@gmail.com	claudio	1	3
3	Javier	Diaz	1234567889	javier_rojoenminor@yeah.c	asd123	1	2
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.auth_permission_id_seq', 48, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.auth_user_id_seq', 1, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.auth_user_user_permissions_id_seq', 1, false);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.django_admin_log_id_seq', 11, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.django_content_type_id_seq', 12, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.django_migrations_id_seq', 18, true);


--
-- Name: pes_app_carrera_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.pes_app_carrera_id_seq', 3, true);


--
-- Name: pes_app_ciudad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.pes_app_ciudad_id_seq', 23, true);


--
-- Name: pes_app_facultad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.pes_app_facultad_id_seq', 3, true);


--
-- Name: pes_app_tipo_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.pes_app_tipo_usuario_id_seq', 6, true);


--
-- Name: pes_app_universidad_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.pes_app_universidad_id_seq', 11, true);


--
-- Name: pes_app_usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: pesuser
--

SELECT pg_catalog.setval('public.pes_app_usuario_id_seq', 3, true);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: pes_app_carrera pes_app_carrera_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_carrera
    ADD CONSTRAINT pes_app_carrera_pkey PRIMARY KEY (id);


--
-- Name: pes_app_ciudad pes_app_ciudad_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_ciudad
    ADD CONSTRAINT pes_app_ciudad_pkey PRIMARY KEY (id);


--
-- Name: pes_app_facultad pes_app_facultad_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_facultad
    ADD CONSTRAINT pes_app_facultad_pkey PRIMARY KEY (id);


--
-- Name: pes_app_tipo_usuario pes_app_tipo_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_tipo_usuario
    ADD CONSTRAINT pes_app_tipo_usuario_pkey PRIMARY KEY (id);


--
-- Name: pes_app_universidad pes_app_universidad_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_universidad
    ADD CONSTRAINT pes_app_universidad_pkey PRIMARY KEY (id);


--
-- Name: pes_app_usuario pes_app_usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_usuario
    ADD CONSTRAINT pes_app_usuario_pkey PRIMARY KEY (id);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX auth_group_name_a6ea08ec_like ON public.auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON public.auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON public.auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON public.auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX auth_user_groups_group_id_97559544 ON public.auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON public.auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON public.auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON public.auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX auth_user_username_6821ab7c_like ON public.auth_user USING btree (username varchar_pattern_ops);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON public.django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON public.django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX django_session_expire_date_a5c62663 ON public.django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX django_session_session_key_c0390e0f_like ON public.django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: pes_app_carrera_id_facultad_id_809f8121; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX pes_app_carrera_id_facultad_id_809f8121 ON public.pes_app_carrera USING btree (facultad_id);


--
-- Name: pes_app_facultad_id_universidad_id_3e6f40ad; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX pes_app_facultad_id_universidad_id_3e6f40ad ON public.pes_app_facultad USING btree (universidad_id);


--
-- Name: pes_app_universidad_id_ciudad_id_bb605d7a; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX pes_app_universidad_id_ciudad_id_bb605d7a ON public.pes_app_universidad USING btree (ciudad_id);


--
-- Name: pes_app_usuario_id_carrera_id_91d225cc; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX pes_app_usuario_id_carrera_id_91d225cc ON public.pes_app_usuario USING btree (carrera_id);


--
-- Name: pes_app_usuario_tipo_usuario_id_9a454495; Type: INDEX; Schema: public; Owner: pesuser
--

CREATE INDEX pes_app_usuario_tipo_usuario_id_9a454495 ON public.pes_app_usuario USING btree (tipo_usuario_id);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES public.auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES public.auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES public.django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES public.auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pes_app_carrera pes_app_carrera_facultad_id_570c7cb6_fk_pes_app_facultad_id; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_carrera
    ADD CONSTRAINT pes_app_carrera_facultad_id_570c7cb6_fk_pes_app_facultad_id FOREIGN KEY (facultad_id) REFERENCES public.pes_app_facultad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pes_app_facultad pes_app_facultad_universidad_id_bbb34eab_fk_pes_app_u; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_facultad
    ADD CONSTRAINT pes_app_facultad_universidad_id_bbb34eab_fk_pes_app_u FOREIGN KEY (universidad_id) REFERENCES public.pes_app_universidad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pes_app_universidad pes_app_universidad_ciudad_id_4c1672d2_fk_pes_app_ciudad_id; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_universidad
    ADD CONSTRAINT pes_app_universidad_ciudad_id_4c1672d2_fk_pes_app_ciudad_id FOREIGN KEY (ciudad_id) REFERENCES public.pes_app_ciudad(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pes_app_usuario pes_app_usuario_carrera_id_7a4279b8_fk_pes_app_carrera_id; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_usuario
    ADD CONSTRAINT pes_app_usuario_carrera_id_7a4279b8_fk_pes_app_carrera_id FOREIGN KEY (carrera_id) REFERENCES public.pes_app_carrera(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: pes_app_usuario pes_app_usuario_tipo_usuario_id_9a454495_fk_pes_app_t; Type: FK CONSTRAINT; Schema: public; Owner: pesuser
--

ALTER TABLE ONLY public.pes_app_usuario
    ADD CONSTRAINT pes_app_usuario_tipo_usuario_id_9a454495_fk_pes_app_t FOREIGN KEY (tipo_usuario_id) REFERENCES public.pes_app_tipo_usuario(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

