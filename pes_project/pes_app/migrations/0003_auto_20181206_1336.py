# Generated by Django 2.1.3 on 2018-12-06 16:36

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pes_app', '0002_auto_20181129_2134'),
    ]

    operations = [
        migrations.RenameField(
            model_name='carrera',
            old_name='id_facultad',
            new_name='facultad',
        ),
        migrations.RenameField(
            model_name='facultad',
            old_name='id_universidad',
            new_name='universidad',
        ),
        migrations.RenameField(
            model_name='universidad',
            old_name='id_ciudad',
            new_name='ciudad',
        ),
        migrations.RenameField(
            model_name='usuario',
            old_name='id_carrera',
            new_name='carrera',
        ),
    ]
