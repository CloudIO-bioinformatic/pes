from django.test import TestCase
from .form import *
from django.utils import timezone




class ciudadFormTest(TestCase):
    def test_renew_form_nombre_field_label(self):
        form = ciudadForm()
        self.assertTrue(form.fields['nombre'].label == None or form.fields['nombre'].label == 'Talca')
        print("Test atributos de la ciudad en el formulario se hizo de forma correcta")

    def test_renew_form_field_help_text(self):
        form = ciudadForm()
        self.assertEqual(form.fields['nombre'].help_text, '')
        print("\n******** Test de Ciudad ***********")
        print("Test help_text en nombre de la ciudad en el formulario se hizo de forma correcta")

######################################################################################

class universidadFormTest(TestCase):
    def test_renew_form_nombre_field_label(self):
        form = universidadForm()
        self.assertTrue(form.fields['nombre'].label == None or form.fields['nombre'].label == 'UTALCA')
        self.assertTrue(form.fields['ciudad'].label == None or form.fields['ciudad'].label == 'Talca')
        print("Test atributos de la universidad en el formulario se hizo de forma correcta")


    def test_renew_form_field_help_text(self):
        form = universidadForm()
        self.assertEqual(form.fields['nombre'].help_text, '')
        self.assertEqual(form.fields['ciudad'].help_text, '')
        print("\n******** Test de Universidad ***********")
        print("Test help_text en nombre de la universidad en el formulario se hizo de forma correcta")
######################################################################################

class facultadFormTest(TestCase):
    def test_renew_form_nombre_field_label(self):
        form = facultadForm()
        self.assertTrue(form.fields['nombre'].label == None or form.fields['nombre'].label == 'Ingenieria')
        self.assertTrue(form.fields['universidad'].label == None or form.fields['universidad'].label == 'UTALCA')
        print("Test atributos de la facultad en el formulario se hizo de forma correcta")


    def test_renew_form_field_help_text(self):
        form = facultadForm()
        self.assertEqual(form.fields['nombre'].help_text, '')
        self.assertEqual(form.fields['universidad'].help_text, '')
        print("\n******** Test de Facultad ***********")
        print("Test help_text en nombre de la facultad en el formulario se hizo de forma correcta")
######################################################################################
class carreraFormTest(TestCase):
    def test_renew_form_nombre_field_label(self):
        form = carreraForm()
        self.assertTrue(form.fields['nombre'].label == None or form.fields['nombre'].label == 'Medicina')
        self.assertTrue(form.fields['facultad'].label == None or form.fields['facultad'].label == 'Ingenieria')
        print("Test atributos de la carrera en el formulario se hizo de forma correcta")


    def test_renew_form_field_help_text(self):
        form = carreraForm()
        self.assertEqual(form.fields['nombre'].help_text, '')
        self.assertEqual(form.fields['facultad'].help_text, '')
        print("\n******** Test de Carrera ***********")
        print("Test help_text en nombre de la carrera en el formulario se hizo de forma correcta")
######################################################################################

class tipo_usuarioFormTest(TestCase):
    def test_renew_form_nombre_field_label(self):
        form = tipo_usuarioForm()
        self.assertTrue(form.fields['nombre'].label == None or form.fields['nombre'].label == 'Alumno')
        print("Test atributos de tipo_usuario en el formulario se hizo de forma correcta")


    def test_renew_form_field_help_text(self):
        form = tipo_usuarioForm()
        self.assertEqual(form.fields['nombre'].help_text, '')
        print("\n******** Test de tipo_usuario ***********")
        print("Test help_text en nombre de tipo_usuario en el formulario se hizo de forma correcta")

######################################################################################
class usuarioFormTest(TestCase):

    def test_renew_form_nombre_field_label(self):
        form = usuarioForm()
        self.assertTrue(form.fields['nombre'].label == None or form.fields['nombre'].label == 'elraul')
        self.assertTrue(form.fields['apellidos'].label == None or form.fields['apellidos'].label == 'rojas')
        self.assertTrue(form.fields['rut'].label == None or form.fields['rut'].label == '22878987')
        self.assertTrue(form.fields['correo'].label == None or form.fields['correo'].label == 'elbartojoma')
        self.assertTrue(form.fields['contrasena'].label == None or form.fields['contrasena'].label == '1235')
        self.assertTrue(form.fields['carrera'].label == None or form.fields['carrera'].label == 'Medicina')
        self.assertTrue(form.fields['tipo_usuario'].label == None or form.fields['tipo_usuario'].label == 'Alumno')
        print("Test atributos de usuario en el formulario se hizo de forma correcta")


    def test_renew_form_field_help_text(self):
        form = usuarioForm()
        self.assertEqual(form.fields['nombre'].help_text, '')
        print("\n******** Test de Usuario ***********")
        #print("Test help_text en nombre de usuario en el formulario se hizo de forma correcta")
        self.assertEqual(form.fields['apellidos'].help_text, '')
        #print("Test help_text en apellidos de usuario en el formulario se hizo de forma correcta")
        self.assertEqual(form.fields['rut'].help_text, '')
        #print("Test help_text en rut de usuario en el formulario se hizo de forma correcta")
        self.assertEqual(form.fields['correo'].help_text, '')
        #print("Test help_text en correo de usuario en el formulario se hizo de forma correcta")
        self.assertEqual(form.fields['contrasena'].help_text, '')
        print("Test help_text en atributos de usuario en el formulario se hizo de forma correcta")
######################################################################################

class moduloFormTest(TestCase):
    def test_renew_form_nombre_field_label(self):
        form = moduloForm()
        self.assertTrue(form.fields['nombre'].label == None or form.fields['nombre'].label == 'Ingenieria')
        self.assertTrue(form.fields['carrera'].label == None or form.fields['carrera'].label == 'Ingenieria')
        print("Test atributos del modulo en el formulario se hizo de forma correcta")


    def test_renew_form_field_help_text(self):
        form = moduloForm()
        self.assertEqual(form.fields['nombre'].help_text, '')
        self.assertEqual(form.fields['carrera'].help_text, '')
        print("\n******** Test de Modulo ***********")
        print("Test help_text en atributos del modulo en el formulario se hizo de forma correcta")

######################################################################################

class tipo_evaluacionFormTest(TestCase):
    def test_renew_form_nombre_field_label(self):
        form = tipo_evaluacionForm()
        self.assertTrue(form.fields['nombre'].label == None or form.fields['nombre'].label == 'prueba')
        print("Test atributos de tipo_evaluacion en el formulario se hizo de forma correcta")


    def test_renew_form_field_help_text(self):
        form = tipo_evaluacionForm()
        self.assertEqual(form.fields['nombre'].help_text, '')
        print("\n******** Test de tipo_evaluacion ***********")
        print("Test help_text en nombre de tipo_evaluacion en el formulario se hizo de forma correcta")

######################################################################################

class evaluacionFormTest(TestCase):

    def test_renew_form_nombre_field_label(self):
        form = evaluacionForm()
        self.assertTrue(form.fields['nota_final'].label == None or form.fields['nota_final'].label == 6.5)
        self.assertTrue(form.fields['modulo'].label == None or form.fields['modulo'].label == 'Ingenieria')
        self.assertTrue(form.fields['ponderacion'].label == None or form.fields['ponderacion'].label == 40)
        self.assertTrue(form.fields['fecha'].label == None or form.fields['fecha'].label == timezone.now())
        self.assertTrue(form.fields['tipo_evaluacion'].label == None or form.fields['tipo_evaluacion'].label == 'prueba')
        print("Test atributos de evaluacion en el formulario se hizo de forma correcta")


    def test_renew_form_field_help_text(self):
        form = evaluacionForm()
        self.assertEqual(form.fields['nota_final'].help_text, '')
        print("\n******** Test de Evaluacion ***********")

        self.assertEqual(form.fields['modulo'].help_text, '')
        #print("Test help_text en apellidos de usuario en el formulario se hizo de forma correcta")
        self.assertEqual(form.fields['ponderacion'].help_text, '')
        #print("Test help_text en rut de usuario en el formulario se hizo de forma correcta")
        self.assertEqual(form.fields['fecha'].help_text, '')
        #print("Test help_text en correo de usuario en el formulario se hizo de forma correcta")
        self.assertEqual(form.fields['tipo_evaluacion'].help_text, '')
        print("Test help_text en atributos de evaluacion en el formulario se hizo de forma correcta")
######################################################################################

class itemFormTest(TestCase):

    def test_renew_form_nombre_field_label(self):
        form = itemForm()
        self.assertTrue(form.fields['descripcion'].label == None or form.fields['descripcion'].label == 'esta_es_descripcion')
        self.assertTrue(form.fields['puntaje'].label == None or form.fields['puntaje'].label == 20)
        self.assertTrue(form.fields['ponderacion'].label == None or form.fields['ponderacion'].label == 40)
        self.assertTrue(form.fields['evaluacion'].label == None or form.fields['evaluacion'].label == 'prueba')
        print("Test atributos de item en el formulario se hizo de forma correcta")


    def test_renew_form_field_help_text(self):
        form = itemForm()
        self.assertEqual(form.fields['descripcion'].help_text, '')
        print("\n******** Test de Item ***********")
        self.assertEqual(form.fields['puntaje'].help_text, '')
        self.assertEqual(form.fields['ponderacion'].help_text, '')
        self.assertEqual(form.fields['evaluacion'].help_text, '')
        print("Test help_text en atributos de item en el formulario se hizo de forma correcta")
######################################################################################
