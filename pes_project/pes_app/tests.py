from django.test import TestCase
from .models import *
from .form import *
from django.urls import reverse
# Create your tests here.
class ciudadModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        ciudad.objects.create(nombre='ciudad')
        print("Ingresa datos se ejecutó correctamente")

    def test_nombre_label(self):
        ciudad_test = ciudad.objects.get(id=1)
        field_label = ciudad_test._meta.get_field('nombre').verbose_name
        self.assertEquals(field_label, 'nombre')
        print("Test label nombre de ciudad se ejecutó correctamente")

    def test_nombre_max_length(self):
        ciudad_max = ciudad.objects.get(id=1)
        max_length = ciudad_max._meta.get_field('nombre').max_length
        self.assertEquals(max_length, 100)
        print("Test max length de nombre de ciudad se ejecutó correctamente")

    def test_ciudad_name_is_nombre(self):
        ciudad_name = ciudad.objects.get(id=1)
        expected_object_name = f'{ciudad_name.nombre}'
        self.assertEquals(expected_object_name, str(ciudad_name))
        print("Test nombre de la ciudad se ejecutó correctamente")

    #def test_get_absolute_url(self):
    #    ciudad_url = ciudad.objects.get(id=1)
        # This will also fail if the urlconf is not defined.
    #    self.assertEquals(ciudad_url.get_absolute_url(), 'ciudad/')

class ciudadFormTest(TestCase):
    def test_renew_form_nombre_field_label(self):
        form = ciudadForm()
        self.assertTrue(form.fields['nombre'].label == None or form.fields['nombre'].label == 'nombre')
        print("Test nombre de la ciudad en el formulario se ejecutó correctamente")

    def test_renew_form_nombre_field_help_text(self):
        form = ciudadForm()
        self.assertEqual(form.fields['nombre'].help_text, '')
        print("Test help_text en nombre de la ciudad en el formulario se ejecutó correctamente")

    def test_renew_form_nombre_ciudad(self):
        form = ciudadForm({'nombre': 'Talca'})
        self.assertTrue(form.is_valid())
        print("Test de validacion del formulario es verdadero/valido")

class ciudadListViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        numero_ciudades = 13
        for id_ciudad in range(numero_ciudades):
            ciudad.objects.create(
                nombre = f'Rancagua {id_ciudad}',
            )
            print("sdadas")
        print("Test de ingresar 13 ciudades se hizo de forma correcta")

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('ciudad'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'ciudad.html')

    def test_pagination_is_ten(self):
        response = self.client.get(reverse('authors'))
        self.assertEqual(response.status_code, 200)
        self.assertTrue('is_paginated' in response.context)
        self.assertTrue(response.context['is_paginated'] == True)
        self.assertTrue(len(response.context['author_list']) == 10)

#def test_view_url_exists_at_desired_location(self):
#    response = self.client.get('/pes_app/template/ciudad')
#    self.assertEqual(response.status_code, 200)
#def test_view_url_accessible_by_name(self):
#    response = self.client.get(reverse('ciudad'))
#    self.assertEqual(response.status_code, 200)

#funciona, pero tira error con los demas, habrá que separarlos??????
