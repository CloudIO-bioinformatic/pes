from django.contrib import admin
from .models import *


# Register your models here.

admin.site.register(ciudad)
admin.site.register(universidad)
admin.site.register(facultad)
admin.site.register(tipo_usuario)
admin.site.register(carrera)
admin.site.register(usuario)

#admin.site.register(usuario)
#admin.site.register(carrera)
#admin.site.register(tipo_usuario)
#admin.site.register(facultad)
#admin.site.register(universidad)
#admin.site.register(ciudad)

