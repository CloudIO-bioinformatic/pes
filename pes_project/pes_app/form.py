from django import forms
from django.forms import ModelForm
from .models import *
#from django.core.validators import validate_email
from django.utils import *

class ciudadForm(forms.Form):
        nombre = forms.CharField(max_length=100, error_messages='')

class universidadForm(forms.Form):
        nombre = forms.CharField(max_length=100)
        ciudad = forms.ModelChoiceField(queryset=ciudad.objects.all())

class facultadForm(forms.Form):
        nombre = forms.CharField(max_length=100)
        universidad = forms.ModelChoiceField(queryset=universidad.objects.all())

class carreraForm(forms.Form):
        nombre = forms.CharField(max_length=100)
        facultad = forms.ModelChoiceField(queryset=facultad.objects.all())

class tipo_usuarioForm(forms.Form):
        nombre = forms.CharField(max_length=100)

class usuarioForm(forms.Form):
        nombre = forms.CharField(max_length=100)
        apellidos = forms.CharField(max_length=100)
        rut = forms.CharField(max_length=10)
        correo = forms.CharField(max_length=25)
        contrasena = forms.CharField(max_length=1000)
        carrera = forms.ModelChoiceField(queryset=carrera.objects.all())
        tipo_usuario = forms.ModelChoiceField(queryset=tipo_usuario.objects.all())

class moduloForm(forms.Form):
        nombre = forms.CharField(max_length=100)
        carrera = forms.ModelChoiceField(queryset=carrera.objects.all())

class tipo_evaluacionForm(forms.Form):
        nombre = forms.CharField(max_length=100)

class evaluacionForm(forms.Form):
        nota_final = forms.DecimalField()
        modulo = forms.ModelChoiceField(queryset=modulo.objects.all())
        ponderacion = forms.IntegerField()
        fecha = forms.CharField(max_length=100)
        tipo_evaluacion = forms.ModelChoiceField(queryset=tipo_evaluacion.objects.all())
class itemForm(forms.Form):
        descripcion =  forms.CharField(max_length=350)
        puntaje = forms.IntegerField()
        ponderacion = forms.IntegerField()
        evaluacion = forms.ModelChoiceField(queryset=evaluacion.objects.all())
