from django.db import models
from django.urls import reverse


# Create your models here.
#creamos la clase u objeto
class ciudad(models.Model):
    nombre = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.nombre
    #def get_absolute_url(self):
    #    return reverse('pes_app.views.details', args=[str(self.id)])

class universidad(models.Model):
    nombre = models.CharField(max_length=100, blank=False, null=False)
    ciudad = models.ForeignKey(ciudad, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre

class facultad(models.Model):
    nombre = models.CharField(max_length=100, blank=False, null=False)
    universidad = models.ForeignKey(universidad, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre

class carrera(models.Model):
    nombre = models.CharField(max_length=100, blank=False, null=False)
    facultad = models.ForeignKey(facultad, on_delete=models.CASCADE)

    def __str__(self):
        return self.nombre

class tipo_usuario(models.Model):
    nombre = models.CharField(max_length=100, blank=False, null=False)

    def __str__(self):
        return self.nombre

class usuario(models.Model):
    #declaramos variables o atributos del objeto
    nombre = models.CharField(max_length=100, blank=False, null=False)
    apellidos = models.CharField(max_length=100,blank=False, null=False)
    rut = models.CharField(max_length=10,blank=False, null=False)
    correo = models.CharField(max_length=25,blank=False, null=False)
    contrasena = models.CharField(max_length=1000,blank=False, null=False)
    carrera = models.ForeignKey(carrera, on_delete=models.CASCADE, null=True)
    tipo_usuario = models.ForeignKey(tipo_usuario, on_delete=models.CASCADE, null=True)

    #declarando orden por defecto de la tabla
    class Meta:
        ordering = ['nombre', '-apellidos']
    def __str__(self):
        return self.nombre

class modulo(models.Model):
    nombre = models.CharField(max_length=100, blank=False, null=False)
    carrera = models.ForeignKey(carrera, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.nombre

class tipo_evaluacion(models.Model):
    nombre = models.CharField(max_length=50, blank=False, null=False)

    def __str__(self):
        return self.nombre

class evaluacion(models.Model):
    #declaramos variables o atributos del objeto
    nota_final = models.DecimalField(decimal_places=1,max_digits=2, blank=True, null=True)
    modulo = models.ForeignKey(modulo, on_delete=models.CASCADE, null=True)
    ponderacion = models.IntegerField(blank=False, null=False)
    fecha = models.DateTimeField(blank=True, null=True)
    tipo_evaluacion = models.ForeignKey(tipo_evaluacion, on_delete=models.CASCADE, null=True)
    #declarando orden por defecto de la tabla
    class Meta:
        ordering = ['fecha']
    def __str__(self):
        return self.tipo_evaluacion.nombre

class item(models.Model):
    descripcion = models.CharField(max_length=50, blank=False, null=False)
    puntaje = models.IntegerField(blank=False, null=False)
    ponderacion = models.IntegerField(blank=False, null=False)
    evaluacion = models.ForeignKey(evaluacion, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.descripcion
