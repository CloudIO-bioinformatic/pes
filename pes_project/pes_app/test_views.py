from django.test import TestCase
from .models import *
from django.urls import reverse
from django.utils import timezone

numero_test=10

######################################################################################
class ciudadViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Ciudad ***********")
        for id_ciudad in range(numero_test):
            ciudad.objects.create(
                nombre = f'Talca {id_ciudad}',
            )
        print("Test de ingresar",numero_test ," ciudades se hizo de forma correcta")

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/ciudad/')
        self.assertEquals(response.status_code, 200)
        print("la url de la vista existe y esta en la ubicacion correcta")

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('ciudad'))
        self.assertEqual(response.status_code, 200)
        print("la url de la vista existe y es accesible correctamente por su nombre")

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('ciudad'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'ciudad.html')
        print("la url de la vista esta utilizando la plantilla html correcta")

######################################################################################
class universidadViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Universidad ***********")
        ciudad_universidad=ciudad.objects.create(nombre='ciudad')
        for id_universidad in range(numero_test):
            universidad.objects.create(
                nombre = f'UTALCA {id_universidad }',
                ciudad = ciudad_universidad,
            )
        print("Test de ingresar",numero_test ," universidades se hizo de forma correcta")

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/universidad/')
        self.assertEquals(response.status_code, 200)
        print("la url de la vista existe y esta en la ubicacion correcta")

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('universidad'))
        self.assertEqual(response.status_code, 200)
        print("la url de la vista existe y es accesible correctamente por su nombre")

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('universidad'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'universidad.html')
        print("la url de la vista esta utilizando la plantilla html correcta")


######################################################################################
class facultadViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Facultad ***********")
        ciudad_universidad=ciudad.objects.create(nombre='Talca')
        universidad_facultad=universidad.objects.create(nombre='UTALCA',ciudad=ciudad_universidad)
        for id_facultad in range(numero_test):
            facultad.objects.create(
                nombre = f'Ingenieria {id_facultad }',
                universidad = universidad_facultad,
            )
        print("Test de ingresar",numero_test ," facultades se hizo de forma correcta")

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/facultad/')
        self.assertEquals(response.status_code, 200)
        print("la url de la vista existe y esta en la ubicacion correcta")

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('facultad'))
        self.assertEqual(response.status_code, 200)
        print("la url de la vista existe y es accesible correctamente por su nombre")

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('facultad'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'facultad.html')
        print("la url de la vista esta utilizando la plantilla html correcta")


######################################################################################
class carreraViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Carrera ***********")
        ciudad_universidad=ciudad.objects.create(nombre='Talca')
        universidad_facultad=universidad.objects.create(nombre='UTALCA',ciudad=ciudad_universidad)
        facultad_carrera=facultad.objects.create(nombre='Ingenieria', universidad=universidad_facultad)
        for id_carrera in range(numero_test):
            carrera.objects.create(
                nombre = f'Medicina {id_carrera }',
                facultad = facultad_carrera,
            )
        print("Test de ingresar",numero_test ," carreras se hizo de forma correcta")

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/carrera/')
        self.assertEquals(response.status_code, 200)
        print("la url de la vista existe y esta en la ubicacion correcta")

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('carrera'))
        self.assertEqual(response.status_code, 200)
        print("la url de la vista existe y es accesible correctamente por su nombre")

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('carrera'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'carrera.html')
        print("la url de la vista esta utilizando la plantilla html correcta")


######################################################################################
class tipoUsuarioViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Tipo de Usuario ***********")
        for id_tipo_usuario in range(numero_test):
            tipo_usuario.objects.create(
                nombre = f'Alumno {id_tipo_usuario}',
            )
        print("Test de ingresar",numero_test ," tipos de usuarios se hizo de forma correcta")

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/tipo_usuario/')
        self.assertEquals(response.status_code, 200)
        print("la url de la vista existe y esta en la ubicacion correcta")

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('tipo_usuario'))
        self.assertEqual(response.status_code, 200)
        print("la url de la vista existe y es accesible correctamente por su nombre")

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('tipo_usuario'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'tipo_usuario.html')
        print("la url de la vista esta utilizando la plantilla html correcta")


######################################################################################
class usuarioViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Usuario ***********")
        tipo_usuario2=tipo_usuario.objects.create(nombre='Alumno')
        ciudad_usuario=ciudad.objects.create(nombre='Talca')
        universidad_usuario=universidad.objects.create(nombre='UTALCA',ciudad=ciudad_usuario)
        facultad_usuario=facultad.objects.create(nombre='Ingenieria', universidad=universidad_usuario)
        carrera_usuario=carrera.objects.create(nombre='Medicina',facultad=facultad_usuario)
        for id_usuario in range(numero_test):
            usuario.objects.create(
                nombre = f'elraul {id_usuario }',
                apellidos='rojas',
                rut='22878987',
                correo='elbartojoma',
                contrasena='1235',
                carrera=carrera_usuario,
                tipo_usuario = tipo_usuario2,
            )
        print("Test de ingresar",numero_test ," usuarios se hizo de forma correcta")

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/usuario/')
        self.assertEquals(response.status_code, 200)
        print("la url de la vista existe y esta en la ubicacion correcta")

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('usuario'))
        self.assertEqual(response.status_code, 200)
        print("la url de la vista existe y es accesible correctamente por su nombre")

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('usuario'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'usuario.html')
        print("la url de la vista esta utilizando la plantilla html correcta")

######################################################################################
class moduloViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Modulo ***********")
        ciudad_universidad=ciudad.objects.create(nombre='Talca')
        universidad_facultad=universidad.objects.create(nombre='UTALCA',ciudad=ciudad_universidad)
        facultad_carrera=facultad.objects.create(nombre='Ingenieria', universidad=universidad_facultad)
        carrera_modulo=carrera.objects.create(nombre='Ingenieria', facultad=facultad_carrera)
        for id_modulo in range(numero_test):
            modulo.objects.create(
                nombre = f'Ingenieria {id_modulo}',
                carrera = carrera_modulo,
            )
        print("Test de ingresar",numero_test ," tipos de modulo se hizo de forma correcta")

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/modulo/')
        self.assertEquals(response.status_code, 200)
        print("la url de la vista existe y esta en la ubicacion correcta")

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('modulo'))
        self.assertEqual(response.status_code, 200)
        print("la url de la vista existe y es accesible correctamente por su nombre")

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('modulo'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'modulo.html')
        print("la url de la vista esta utilizando la plantilla html correcta")


######################################################################################
class tipoEvaluacionViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Tipo de Evaluacion ***********")
        for id_tipo_evaluacion in range(numero_test):
            tipo_evaluacion.objects.create(
                nombre = f'prueba {id_tipo_evaluacion}',
            )
        print("Test de ingresar",numero_test ," tipos de evaluacion se hizo de forma correcta")

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/tipo_evaluacion/')
        self.assertEquals(response.status_code, 200)
        print("la url de la vista existe y esta en la ubicacion correcta")

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('tipo_evaluacion'))
        self.assertEqual(response.status_code, 200)
        print("la url de la vista existe y es accesible correctamente por su nombre")

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('tipo_evaluacion'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'tipo_evaluacion.html')
        print("la url de la vista esta utilizando la plantilla html correcta")


######################################################################################
class evaluacionViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Evaluacion ***********")
        ciudad_universidad=ciudad.objects.create(nombre='Talca')
        universidad_facultad=universidad.objects.create(nombre='UTALCA',ciudad=ciudad_universidad)
        facultad_carrera=facultad.objects.create(nombre='Ingenieria', universidad=universidad_facultad)
        carrera_modulo=carrera.objects.create(nombre='Ingenieria', facultad=facultad_carrera)
        modulo_evaluacion=modulo.objects.create(nombre='Ingenieria', carrera=carrera_modulo)
        tipo_evaluacion_evaluacion=tipo_evaluacion.objects.create(nombre='prueba')
        for id_evaluacion in range(numero_test):
            evaluacion.objects.create(
                nota_final = 6.4 ,
                modulo = modulo_evaluacion,
                ponderacion = 40,
                fecha = timezone.now(),
                tipo_evaluacion = tipo_evaluacion_evaluacion,
            )
        print("Test de ingresar",numero_test ," evaluaciones se hizo de forma correcta")

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/evaluacion/')
        self.assertEquals(response.status_code, 200)
        print("la url de la vista existe y esta en la ubicacion correcta")

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('evaluacion'))
        self.assertEqual(response.status_code, 200)
        print("la url de la vista existe y es accesible correctamente por su nombre")

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('evaluacion'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'evaluacion.html')
        print("la url de la vista esta utilizando la plantilla html correcta")


######################################################################################
class itemViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        print("\n******** Test de Item ***********")
        ciudad_universidad=ciudad.objects.create(nombre='Talca')
        universidad_facultad=universidad.objects.create(nombre='UTALCA',ciudad=ciudad_universidad)
        facultad_carrera=facultad.objects.create(nombre='Ingenieria', universidad=universidad_facultad)
        carrera_modulo=carrera.objects.create(nombre='Ingenieria', facultad=facultad_carrera)
        modulo_evaluacion=modulo.objects.create(nombre='Ingenieria', carrera=carrera_modulo)
        tipo_evaluacion_evaluacion=tipo_evaluacion.objects.create(nombre='prueba')
        evaluacion_item=evaluacion.objects.create(nota_final=7.0, tipo_evaluacion=tipo_evaluacion_evaluacion,
        modulo=modulo_evaluacion, ponderacion=40, fecha=timezone.now())
        for id_item in range(numero_test):
            item.objects.create(
                descripcion = f'esta_es_descripcion {id_item}',
                puntaje = 15,
                ponderacion = 40,
                evaluacion = evaluacion_item,
            )
        print("Test de ingresar",numero_test ," items se hizo de forma correcta")

    def test_view_url_exists_at_desired_location(self):
        response = self.client.get('/item/')
        self.assertEquals(response.status_code, 200)
        print("la url de la vista existe y esta en la ubicacion correcta")

    def test_view_url_accessible_by_name(self):
        response = self.client.get(reverse('item'))
        self.assertEqual(response.status_code, 200)
        print("la url de la vista existe y es accesible correctamente por su nombre")

    def test_view_uses_correct_template(self):
        response = self.client.get(reverse('item'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'item.html')
        print("la url de la vista esta utilizando la plantilla html correcta")


######################################################################################
